package com.ndood.admin.core.exception;

import com.ndood.admin.core.constaints.AdminErrCode;

public class AdminException extends Exception {
	private static final long serialVersionUID = 13168926815908268L;

	private AdminErrCode code;

	public AdminErrCode getCode() {
		return code;
	}

	public void setCode(AdminErrCode code) {
		this.code = code;
	}

	public AdminException(String message) {
		super(message);
	}

	public AdminException(AdminErrCode code) {
        super(code.getValue());
        this.code = code;
    }
	
	public AdminException(AdminErrCode code, String message) {
        super(message);
        this.code = code;
    }

}