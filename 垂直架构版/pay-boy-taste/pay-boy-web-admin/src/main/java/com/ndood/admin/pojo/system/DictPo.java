package com.ndood.admin.pojo.system;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

/**
 * 字典bean
 * @author ndood
 */
@Entity
@Table(name="t_dict")
@Cacheable(true)
@Getter @Setter
public class DictPo implements Serializable{
	private static final long serialVersionUID = 1321576057862598582L;

	/**
	 * ID
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	/**
	 * 编码
	 */
	@Column(name="`code`", length=32, nullable = false, unique = true)
	private String code;
	
	/**
	 * 描述
	 */
	@Column(name="`desc`", length=32)
	private String desc;
	
	/**
	 * 排序
	 */
	@Column(nullable = false)
	private Integer sort;
	
	/**
	 * 删除标记
	 */
	@Column(nullable = false)
	private Integer status;
	
	/**
	 * 创建时间
	 */
	@Column(name="create_time", nullable = false, columnDefinition="datetime", updatable=false)
	private Date createTime;
	
	/**
	 * 修改时间
	 */
	@Column(name="update_time", nullable = false, columnDefinition="datetime")
	private Date updateTime;

	/**
	 * 字典值
	 */
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "dict_id", referencedColumnName = "id")
	private List<DictValuePo> values;

}
