package com.ndood.admin.core.jpa.dialect;
import org.hibernate.dialect.Oracle10gDialect;
import org.hibernate.dialect.function.SQLFunctionTemplate;
import org.hibernate.type.StandardBasicTypes;
 
/**
 * 数据库为ORACLE
 * https://www.cnblogs.com/amCharlie/p/9328658.html
 */
public class OracleChineseDialect extends Oracle10gDialect{
    
    /**
     * 使JPQL支持中文排序
     */
    public OracleChineseDialect(){
        super();
        registerFunction("convert", new SQLFunctionTemplate(StandardBasicTypes.STRING, "nlssort(?1,'NLS_SORT=SCHINESE_PINYIN_M')"));
    }
}