package com.ndood.admin.core.quartz;

import java.io.Serializable;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.ndood.admin.pojo.system.JobPo;

import lombok.Getter;
import lombok.Setter;
@Getter @Setter
public class ScheduleJob implements Serializable ,Job {
	private static final long serialVersionUID = -7768955978747849651L;
	public static final String STATUS_RUNNING = "1";
	public static final String STATUS_NOT_RUNNING = "0";
	public static final String CONCURRENT_IS = "1";
	public static final String CONCURRENT_NOT = "0";
	/**
	 * 任务名称
	 */
	private String jobName;
	/**
	 * 任务分组
	 */
	private String jobGroup;
	/**
	 * 任务状态 是否启动任务
	 */
	private String jobStatus;
	/**
	 * cron表达式
	 */
	private String cronExpression;
	/**
	 * 描述
	 */
	private String description;
	/**
	 * 任务执行时调用哪个类的方法 包名+类名
	 */
	private String beanClass;
	/**
	 * 任务是否有状态
	 */
	private String isConcurrent;
	/**
	 * Spring bean
	 */
	private String springBean;
	/**
	 * 任务调用的方法名
	 */
	private String methodName;
	@Override
	public void execute(JobExecutionContext context) throws JobExecutionException {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * 将job转换成
	 */
	public static ScheduleJob transfer(JobPo job) {
		ScheduleJob scheduleJob = new ScheduleJob();
		scheduleJob.setBeanClass(job.getBeanClass());
		scheduleJob.setCronExpression(job.getCronExpression());
		scheduleJob.setDescription(job.getDescription());
		scheduleJob.setIsConcurrent(job.getIsConcurrent());
		scheduleJob.setJobName(job.getJobName());
		scheduleJob.setJobGroup(job.getJobGroup());
		scheduleJob.setJobStatus(job.getJobStatus());
		scheduleJob.setMethodName(job.getMethodName());
		scheduleJob.setSpringBean(job.getSpringBean());
		return scheduleJob;
	}
}