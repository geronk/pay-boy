package com.ndood.admin.service.system.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import com.ndood.admin.pojo.comm.dto.DataTableDto;
import com.ndood.admin.pojo.system.LogPo;
import com.ndood.admin.pojo.system.dto.LogDto;
import com.ndood.admin.pojo.system.query.LogQuery;
import com.ndood.admin.repository.system.LogRepository;
import com.ndood.admin.repository.system.manager.LogRepositoryManager;
import com.ndood.admin.service.system.SystemLogService;

import lombok.extern.slf4j.Slf4j;

/**
 * 日志模块业务类
 */
@Service
@Slf4j
public class SystemLogServiceImpl implements SystemLogService {

	@Autowired
	private LogRepositoryManager logRepositoryManager;
	
	@Autowired
	private LogRepository logsDao;
	
	@Override
	public void makeLog(LogPo log) {
		logsDao.save(log);
	}
	
	@Override
	public DataTableDto pageLogList(LogQuery query) throws Exception {

		log.debug("Step1: 封装查询参数"); 
		Integer pageSize = query.getLimit();
		String keywords = query.getKeywords();
		Integer pageNo = query.getPageNo();
		Date startTime = query.getStartTime();
		Date endTime = query.getEndTime();
		
		log.debug("Step2: 分页查询出轮播图数据并返回");
		Page<LogDto> page = logRepositoryManager.pageLogList(keywords, startTime, endTime, pageNo, pageSize);
		return new DataTableDto(page.getContent(), page.getTotalElements());
		
	}
	
	/*public DataTableDto pageLogsList(LogQuery query) throws Exception {
		// Step1: 封装查询参数
		Integer pageSize = query.getLimit();
		String keywords = query.getKeywords();
		Integer pageNo = query.getPageNo();
		Date startTime = query.getStartTime();
		Date endTime = query.getEndTime();
		
		List<Sort.Order> orders = new ArrayList<Sort.Order>();
		Sort.Order order = new Sort.Order(Sort.Direction.ASC, "sort");
		orders.add(order);
		Pageable pageable = new PageRequest(pageNo, pageSize, new Sort(orders));
		
		// 获取当前用户的信息
		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String username = userDetails.getUsername();

		// Step2: 进行复杂查询
		Page<LogPo> page = logsDao.findAll(new Specification<LogPo>() {
			@Override
			public Predicate toPredicate(Root<LogPo> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
				Predicate p = cb.conjunction();
				if (!StringUtils.isEmpty(keywords)) {
					Predicate temp = cb.or(
						cb.like(root.get("name"), "%" + keywords + "%"),
						cb.like(root.get("desc"), "%" + keywords + "%"));
					p = cb.and(p,temp);
				}
				if(startTime!=null){
					p = cb.and(cb.greaterThanOrEqualTo(root.get("createTime"), startTime), p);
				}
				if(endTime!=null){
					p = cb.and(cb.lessThan(root.get("createTime"), endTime), p);
				}
				p = cb.and(cb.equal(root.get("username"), username), p);
				return p;
			}
		}, pageable);
		
		// Step3: 转换成dto
		List<RoleDto> list = new ArrayList<RoleDto>();
		for (LogPo po : page.getContent()) {
			RoleDto dto = new RoleDto();
			JPAUtil.fatherToChild(po, dto);
			list.add(dto);
		}
		return new DataTableDto(list, page.getTotalElements());
	}*/

}
