package com.ndood.admin.core.properties;

import lombok.Data;

/**
 * 阿里云短信相关配置
 */
@Data
public class AliyunSmsProperties {

	private String accessKeyId = "onD5ToyxxxxxxedM";

	private String secretKeyId = "US7s7dRxxxxxxNRK0KZDBLNH8Suv1b";

	private String domain = "dysmsapi.aliyuncs.com";
	
	private String product = "Dysmsapi";
	
	private String endPoint = "cn-hangzhou";
	
	private String signName = "xxxx";
	
	private String templateCode = "SMS_11111111";

	private String template = "{\"code\":\"%s\", \"phone\":\"%s\"}";

}
