package com.ndood.admin.repository.system;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import com.ndood.admin.pojo.system.JobPo;
/**
 * 日志dao
 */
public interface JobRepository extends JpaRepository<JobPo, Integer>{
	Page<JobPo> findAll(Specification<JobPo> specification, Pageable pageable);
	List<JobPo> findByJobStatus(String jobStatus);
	
}