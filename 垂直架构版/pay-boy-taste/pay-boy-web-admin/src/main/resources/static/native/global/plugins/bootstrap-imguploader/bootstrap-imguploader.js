(function($) {
	"use strict";
	$.fn.ndoodImgUploader = function(options, param, param2) {
		// 如果options为方法，则调用字符串对应的方法
		if (typeof options == 'string') {
			return $.fn.ndoodImgUploader.methods[options](this, param, param2);
		}

		// 如果options为json，则根据json进行初始化
		options = $.extend({}, $.fn.ndoodImgUploader.defaults, options || {});
		
		var target = $(this);
		target.prop('temp_options',options);
		
		$.fn.ndoodImgUploader.init(target, options);
		return target;
	};
	
	// 默认配置
	$.fn.ndoodImgUploader.defaults = {
		url: '', // 上传url
		method: 'post', // 传输方式
		fileType: ["jpg","png","bmp","jpeg"],  // 上传文件的类型
		fileSize: 1024 * 1024 * 10,  // 上传文件的大小 10M
		imgNum: 1, // 图片数量
		imgName: 'imgName',
		errorTips: function(msg){ // 错误提示方式
			alert(msg);
		}
	};
	
	$.fn.ndoodImgUploader.makeNewSection = function(options, img_url){
		// 图片名称
		var name = '';
		if(img_url!=null && img_url != '' && img_url != undefined){
			var lindex = img_url.lastIndexOf("/");
			name = img_url.substring(lindex+1);
		}
		
		// section div
		var $section = $("<div class='image-upload-section up loading'>");
		
		if(options.width!=undefined){
			$section.css('width',options.width);
		}
		if(options.height!=undefined){
			$section.css('height',options.height);
		}
		
		// mask标签鼠标移动，高亮显示
		var $span = $("<span class='image-upload-mask'>");
		$span.appendTo($section);
		
		// 关闭按钮
		var $close_btn = $("<span class='image-upload-close'>").on("click",function(event){
			// TODO 同时删除files
			var container = $(this).parents(".image-upload");
			$(this).parent().remove();
			var num_up = container.find(".image-upload-section.up").length;
			if(num_up < options.imgNum){
				container.find('.image-upload-file').parent().show();
			}
			event.preventDefault();
			event.stopPropagation();
		});
		$close_btn.appendTo($section);
		
		// 上传好的图片
		var $img = $("<img class='image-upload-img opcity'>");
		$img.attr("src", img_url).appendTo($section);

		var $name = $("<p class='image-upload-img-name'>");
		$name.html(name).appendTo($section);
		
		var $url = $("<input name='"+options.imgName+"' type='hidden'>");
		$url.val(img_url).appendTo($section);
		
		return $section;
	}
	
	// 更新section中的url和name
	$.fn.ndoodImgUploader.updateSection = function(section, img_url){
		// 图片名称
		var name = '';
		if(img_url!=null && img_url != '' && img_url != undefined){
			var lindex = img_url.lastIndexOf("/");
			name = img_url.substring(lindex+1);
		}
		section.find(".image-upload-img").attr("src", img_url);
		section.find("input[type='hidden']").val(img_url);
		section.find(".image-upload-img-name").html(name);
	}
	
	// 初始化combotree
	$.fn.ndoodImgUploader.init = function(target, options){
		// 补充样式
		if(!target.hasClass('image-upload')){
			target.addClass('image-upload');
		}

		// 可支持的图片格式
		var accept_arr = [];
		for(var i = 0; i<options.fileType.length; i++){
			accept_arr.push("image/"+options.fileType[i]);
		}
		var accept = options.fileType.join(",");

		// 创建section
		var section = $('<div class="image-upload-section">'+
				'	<div class="image-upload-btn"></div>'+
				'	<input type="file" class="image-upload-file" value="" accept="'+accept+'" multiple="">'+
				'</div>');

		if(options.width!=undefined){
			section.css('width',options.width);
		}
		if(options.height!=undefined){
			section.css('height',options.height);
		}
		target.html('');
		target.append(section);
		
		// 获取添加按钮
		var add_btn = section.find(".image-upload-file");
		
		/* 点击图片的文本框 */
		add_btn.change(function(){
			// 获取上传的图片文件
			var file_list = $(this).prop('files');
			// console.log(file_list) // _1_zhangmingen_thumb.jpg 49639
			
			// 遍历得到的图片文件
			var old_section_length = target.find(".image-upload-section.up").length;
			
			// 总的图片数量
			var total_length = old_section_length + file_list.length; 
			
			// 一次选择上传超过max个 或者是已经上options.imgNum次上传的到的总数也不可以超过max个
			if(file_list.length > options.imgNum || total_length > options.imgNum ){
				options.errorTips("上传图片数目不可以超过"+options.imgNum+"个，请重新选择");
				return;
			}
			
			// 校验图片是否合法
			if(!$.fn.ndoodImgUploader.validate(options, file_list)){
				return;
			}
			
			// 显示加载中效果
			var temp_sections = [];
			for(var i=0; i < file_list.length; i++){
				var $section = $.fn.ndoodImgUploader.makeNewSection(options, '');
				//target.prepend($section);
				target.find('.image-upload-file').parent().before($section);
				temp_sections.push($section);
			}
			// 如果上传文件的个数>=max，则将上传按钮隐藏
			var num_up = target.find(".image-upload-section.up").length;
			if(num_up >= options.imgNum){
				target.find('.image-upload-file').parent().hide();
			}
			
			// 创建表单提交
			var formData = new FormData();
			for(var i=0; i < $(this)[0].files.length; i++){
				formData.append("file"+i, $(this)[0].files[i]);
			}

			$.ajax({
				url: options.url,  
				type: options.method,
				data: formData,  
				cache: false,
				processData: false,
				contentType: false,
				async: true,
				success: function(data){
					if(data.code!='10000'){
						// 如果上传失败，将所有temp_sections删除掉
						for(var i = 0; i < temp_sections.length; i++){
							temp_sections[i].remove();
						}
						// 如果上传文件的个数<max，则将上传按钮恢复
						var num_up = target.find(".image-upload-section.up").length;
						if(num_up < options.imgNum){
							target.find('.image-upload-file').parent().show();
						}
						options.errorTips(data.desc);
						
						$(add_btn)[0].value="";
						return;
					}
					
					// 将返回的url都设置到空的section中
					var img_urls = data.data.split(',');
					for(var i = 0; i < img_urls.length; i++){
						$.fn.ndoodImgUploader.updateSection(temp_sections[i], img_urls[i]);
					}
					
					// 取消加载中效果，显示图片
					target.find(".image-upload-section").removeClass("loading");
					target.find(".image-upload-img").removeClass("opcity");
					
					// input内容清空
					$(add_btn)[0].value=""; 
				}
			});
		});
		
		// 获取初始值
		var init_value = target.attr('init_value');
		init_value = init_value == undefined ? '' : init_value;
		//target.removeAttr("init_value");

		// 初始化
		if(init_value!=''){
			// 依次生成初始化图片标签
			var img_arr = init_value.split(',');
			for(var i=0; i < img_arr.length && i<options.imgNum; i++){
				var $section = $.fn.ndoodImgUploader.makeNewSection(options, img_arr[i]);
				//target.append($section);
				target.find('.image-upload-file').parent().before($section);
				target.find(".image-upload-section").removeClass("loading");
				target.find(".image-upload-img").removeClass("opcity");
			}
			// 如果上传文件的个数>=max，则将上传按钮隐藏
			var num_up = target.find(".image-upload-section.up").length;
			if(num_up >= options.imgNum){
				target.find('.image-upload-file').parent().hide();
			}
		}
	}
	
	// 对上传的图片进行校验
	$.fn.ndoodImgUploader.validate = function(options, files){
		for(var i = 0, file; file = files[i]; i++){
			// 获取文件上传的后缀名
			var newStr = file.name.split("").reverse().join("");
			if(newStr.split(".")[0] == null){
				options.errorTips(file.name +'没有类型, 无法识别');	
				return false;
			}
			var type = newStr.split(".")[0].split("").reverse().join("");
			if(jQuery.inArray(type, options.fileType) < 0){
				options.errorTips(file.name +'上传类型不符合');	
				return false;
			}
			if (file.size >= options.fileSize) {
				options.errorTips(file.name +'图片不能大于'+ parseInt(options.fileSize/1024/1024) + 'M');
				return false;
			}
		}
		return true;
	}
	
	// 自定义方法列表
	$.fn.ndoodImgUploader.methods = {
		reload: function(target){
			var options = target.prop('temp_options');
			$.fn.ndoodImgUploader.init(target, options);
		}
	};
})(jQuery);