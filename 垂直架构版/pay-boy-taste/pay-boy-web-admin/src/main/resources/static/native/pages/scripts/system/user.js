define([
	'jquery',
	'jquery-validation',
	'jquery-extension',
	'layui',
	'bootstrap-datetimepicker',
	'bootstrap-datetimepicker-zh-CN',
	'bootstrap-datepicker',
	'bootstrap-datepicker-zh-CN',
	'bootstrap-table',
	'bootstrap-table-zh-CN',
	'bootstrap-table-mobile',
	'jquery-jstree',
	'jquery-combotree',
	'bootstrap-imguploader',
	'jquery-distselector'
], function() {
	return {
		initUpdateDialog: function(){
			// 出生日期
			$('#user_update_birthday').datepicker({
				language: 'zh-CN',
				format: 'yyyy-mm-dd',
				autoclose: true
			});
			
			// 部门树
			$("#user_update_dept_combotree").ndoodComboTree({
				url:'/system/department/department_tree',
				key:'deptId',
				placeholder:'部门'
			});
			
			// 初始化地区三级联动组件
			$("#user_update_province_id").ndoodDistSelector({
				provinceId : 'user_update_province_id',
				cityId : 'user_update_city_id',
				regionId : 'user_update_district_id',
				id : 'sid',
				name : 'name',
				url : '/system/region/province'
			});
			
			// 图片上传插件
			$("#user_update_head_image").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				imgName: 'headImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 表单验证
			$("#user_update_form").validate({
				rules: {
					nickName:{
						required: true,
						rangelength:[2,18],
						isNickName:true,
					},
					deptId:{
						required: true,
					},
					mobile:{
						required: true,
						isMobile: true,
					},
					email:{
						required: true,
						isEmail: true,
					},
					address:{
						rangelength:[0,40],
					},
					roleIds:{
						required: true,
					},
					sort:{
						required: true,
					},
				},
				messages:{
					nickName:{
						required: "用户昵称不能为空.",
						rangelength: $.validator.format("昵称最小长度:{0}, 最大长度:{1}。"),
						isNickName:"用户名必须由字母、数字或特殊符号(@_.)组成.",
					},
					deptId:{
						required: "部门不能为空.",
					},
					mobile:{
						required: "手机号不能为空.",
						isMobile: "请正确填写您的手机号码.",
					},
					email:{
						required: "邮箱不能为空.",
						isEmail: "请正确填写你的邮箱.",
					},
					address:{
						rangelength: $.validator.format("详细地址最小长度:{0}, 最大长度:{1}。"),
					},
					roleIds:{
						required: '角色不能为空.',
					},
					sort:{
						required: "排序字段不能为空.",
					},
				},
				errorPlacement: function(error,element) {
					if(element.attr("name")=="deptId"){
						$(element).parent().after(error);
						return;
					}
					if(element.is(":checkbox")){
						$(element).parent().parent().parent();
						$(element).parent().parent().after(error);
						return;
					}
					$(element).parent().addClass('has-error');
					$(element).after(error);
				},
				success:function(element) {
					$(element).parent().removeClass('has-error');
					$(element).parent().find('label.error').remove();
				},
				// 开启隐藏域验证
				ignore: [],
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
			});
		},
		initAddDialog: function(){
			// 出生日期
			$('#user_add_birthday').datepicker({
				language: 'zh-CN',
				format: 'yyyy-mm-dd',
				autoclose: true
			});	
			
			// 部门树
			$("#user_add_dept_combotree").ndoodComboTree({
				url:'/system/department/department_tree',
				key:'deptId',
				placeholder:'部门'
			});
			
			// 初始化地区三级联动组件
			$("#user_add_province_id").ndoodDistSelector({
				provinceId : 'user_add_province_id',
				cityId : 'user_add_city_id',
				regionId : 'user_add_district_id',
				id : 'sid',
				name : 'name',
				url : '/system/region/province'
			});
			
			// 图片上传插件
			$("#user_add_head_image").ndoodImgUploader({
				url: '/oss/aliyun/upload',
				fileType: ["jpg","png","bmp","jpeg"], 
				fileSize: 1024 * 1024 * 10,
				imgNum: 1,
				imgName: 'headImgUrl',
				errorTips: function(msg){
					layer.alert(msg);
				}
			});
			
			// 表单验证
			$("#user_add_form").validate({
				rules: {
					nickName:{
						required: true,
						rangelength:[2,18],
						isNickName:true,
					},
					deptId:{
						required: true,
					},
					mobile:{
						required: true,
						isMobile: true,
					},
					email:{
						required: true,
						isEmail: true,
					},
					address:{
						rangelength:[0,40],
					},
					roleIds:{
						required: true,
					},
					sort:{
						required: true,
					},
				},
				messages:{
					nickName:{
						required: "用户昵称不能为空.",
						rangelength: $.validator.format("昵称最小长度:{0}, 最大长度:{1}。"),
						isNickName:"用户名必须由字母、数字或特殊符号(@_.)组成.",
					},
					deptId:{
						required: "部门不能为空.",
					},
					mobile:{
						required: "手机号不能为空.",
						isMobile: "请正确填写您的手机号码.",
					},
					email:{
						required: "邮箱不能为空.",
						isEmail: "请正确填写你的邮箱.",
					},
					address:{
						rangelength: $.validator.format("详细地址最小长度:{0}, 最大长度:{1}。"),
					},
					roleIds:{
						required: '角色不能为空.',
					},
					sort:{
						required: "排序字段不能为空.",
					},
				},
				errorPlacement: function(error,element) {
					if(element.attr("name")=="deptId"){
						$(element).parent().after(error);
						return;
					}
					if(element.is(":checkbox")){
						$(element).parent().parent().parent();
						$(element).parent().parent().after(error);
						return;
					}
					$(element).parent().addClass('has-error');
					$(element).after(error);
				},
				success:function(element) {
					$(element).parent().removeClass('has-error');
					$(element).parent().find('label.error').remove();
				},
				// 开启隐藏域验证
				ignore: [],
			    // 是否在提交表单时验证
				onsubmit: false,
				// 是否在获取焦点时验证
				onfocusout:false,
				// 是否在敲击键盘时验证
				onkeyup:false,
				// 是否在点击时验证
				onclick: false,
				// 提交表单后，（第一个）未通过验证的表单获得焦点
				focusInvalid:true,
				// 当未通过验证的元素获得焦点时，移除错误提示
				focusCleanup:false,
			});
		},
		initPage: function(){
			// 1 初始化日期控件
			$('#user_start_date').datetimepicker({
				language: 'zh-CN',
				format: 'yyyy-mm-dd hh:ii:00',
				autoclose: true
			});
			
			$('#user_end_date').datetimepicker({
				language: 'zh-CN',
				format: 'yyyy-mm-dd hh:ii:00',
				autoclose: true
			});	
			
			$("#user_dept_combotree").ndoodComboTree({
				url:'/system/department/department_tree',
				key:'deptId',
				placeholder:'请选择'
			});
			
			// 2 初始化表单控件
			$('#user_datatable').bootstrapTable({
				url : "/system/user/query", 
				method : 'post', 
				toolbar : '#user_toolbar',
				uniqueId: "id",
				striped: true,
				cache: false,
				pagination : true, 
				sortable: false,
				singleSelect : false,
				search: true,
				showColumns: true,
				showToggle:false,
				cardView: false,
				showRefresh: true,
				clickToSelect: true,
				minimumCountColumns: 5,
				queryParams : function(params) {
					return {
						limit : params.limit,
						offset : params.offset,
						keywords: params.search,
						sortName:'sort',
						nickName: $("#user_content").find("form").find("input[name='nickName']").val(),
						sex: $("#user_content").find("form").find("select option:selected").val(),
						deptId: $("#user_content").find("form").find("input[name='deptId']").val(),
						startTime: $("#user_content").find("form").find("input[name='startTime']").val(),
						endTime: $("#user_content").find("form").find("input[name='endTime']").val()
					};
				},
				sidePagination : "server",
				pageNumber:1, 
				pageSize : 10,
				pageList: [10, 20, 50],
				columns : [
					{
						checkbox : true
					},
					{
						field : 'id',
						title : '编号'
					},
					{
						field : 'nickName',
						title : '昵称',
					},
					{
						field : 'sex',
						title : '性别',
						formatter: function(value, row, index){
							if(value==1){
								return '男';
							}else{
								return '女';
							}
						}
					},
					{
						field : 'birthday',
						title : '出生日期'
					},
					{
						field : 'email',
						title : '邮箱'
					},
					{
						field : 'mobile',
						title : '手机'
					},
					{
						field : 'departmentName',
						title : '部门'
					},
					{
						field : 'sort',
						title : '排序',
					},
					{
						field : 'status',
						title : '状态',
						formatter: function(value, row, index){
							if(value==0){
								return '<span class="text-danger"><i class="fa fa-circle"></i> 禁用</span>';
							}else{
								return '<span class="text-success"><i class="fa fa-circle"></i> 启用</span>';
							}
						}
					},
					{
						field : 'createTime',
						width : '150px',
						title : '创建时间'
					},
					{
						visible : false,
						field : 'updateTime',
						width : '150px',
						title : '最后修改时间'
					},
					{
						title : '操作',
						field : '',
						align : 'center',
						formatter : function(value, row, index) {
							var b1 = '<a href="javascript:void(0);" class="btn btn-xs btn-success btn-editone" onclick="user_update('+row['id']+')"><i class="fa fa-pencil"></i></a>';
							var b2 = '<a href="javascript:;" class="btn btn-xs btn-success btn-delone" onclick="user_delete('+row['id']+')"><i class="fa fa-trash"></i></a>';
							return b1 + b2;
						}
					} 
				]
			});
			
			// 3初始化手机响应式
			var docW = window.innerWidth;
			if (docW < 768) {
				$("#user_search_plus").show();
				$("#user_search_plus_btn").hide();
				$("#user_content .bootstrap-table .fixed-table-toolbar input[type='text']").hide();
			}else{
				$("#user_search_plus").hide();
			}
		}
	}
});

// 添加用户
function user_add() {
	Layout.reloadAjaxContent('/system/user/add', $(".page-sidebar").find("a[href='/system/user']"));
}

//删除用户
function user_delete(id){
	layer.confirm('确定要删除吗？', {
		btn : [ '确定', '取消' ]
	}, function() {
		$.ajax({
			url : '/system/user/delete',
			type : "post",
			data : {'id' : id},
			success : function(data) {
				if (data.code != '10000') {
					layer.alert(data.msg,{icon: 2});
					return;
				}
				layer.alert(data.msg, {icon: 1});
				user_reload();
			},
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
		});
	})
}

// 批量删除
function user_batch_delete(){
	var rows = $('#user_datatable').bootstrapTable('getSelections'); 
	if (rows.length == 0) {
		layer.alert("请至少选择一条记录!",{icon: 7});
		return;
	}
	layer.confirm("确定要删除选中的'" + rows.length + "'条记录吗?", {
		btn : [ '确定', '取消' ]
	}, function() {
		var ids = [];
		$.each(rows, function(i, row) {
			ids.push(row['id']);
		});
		$.ajax({
			type : 'post',
			data : {ids:ids},
			dataType : 'json',
			url : '/system/user/batch_delete',
			success : function(data) {
				if (data.code != '10000') {
					layer.alert(data.msg,{icon: 2});
					return;
				}
				layer.alert(data.msg, {icon: 1});
				user_reload();
			},
	        error: function (response, ajaxOptions, thrownError) {
	        	errorCallBack(response, ajaxOptions, thrownError);                
	        }
		});
	}, function() {});
}

// 修改用户
function user_update(id){
	Layout.reloadAjaxContent('/system/user/update?id='+id, $(".page-sidebar").find("a[href='/system/user']"));
}

//高级查询
function user_search_submit(){
	$('#user_datatable').bootstrapTable('refresh', null);
}

//显示高级搜索
function user_search_plus(){
	var search = $('#user_datatable').parents('.bootstrap-table').find('.fixed-table-toolbar').find("input[type='text']");
	if(!$(search).is(":hidden")){
		// 隐藏普通搜索
		$(search).val('').hide();
		// 显示高级搜索
		$("#user_search_plus").show();
	}else{
		$("#user_search_plus").find("form")[0].reset();
		$("#user_search_plus").hide();
		$(search).show();
	}
}

// 重新加载datatable
function user_reload(){
	$('#user_datatable').bootstrapTable('refresh', null);
}

//保存用户
function add_user(callback){
	// Step1: 表单验证
	// 校验基本表单
	var valid = $("#user_add_form").valid();
	// 校验省市区
	var district_valid = true;
	$("#user_add_province_id").parent().parent().find("label.error_tips").hide();
	var province = $("#user_add_province_id").find("option:checked").val();
	var city = $("#user_add_city_id").find("option:checked").val();
	var district = $("#user_add_district_id").find("option:checked").val();
	if(province==''|city==''|district==''){
		$("#user_add_province_id").parent().parent().find("label.error_tips").show();
		district_valid = false;
	}
	if(!valid | !district_valid){
		return;
	}
	
	// Step2: 提交表单
	var data = $('#user_add_form').serializeJson();
	var roleIds = data['roleIds'];
	if(!(roleIds instanceof Array)){
		var arr = [];
		arr.push(roleIds);
		data['roleIds'] = arr;
	}
	console.log(data);
	
    $.ajax({
    	url:"/system/user/add",
        type:"post",
        dataType:'json',
        contentType:"application/json;charset=utf-8",
        data:JSON.stringify(data),
        async:false,
        success:function(data){
        	if(data.code!='10000'){
        		parent.layer.alert(data.msg, {icon: 1});
        		return;
        	}
        	//layer.alert("添加用户成功.");
        	//Layout.reloadAjaxContent('/system/user', $(".page-sidebar").find("a[href='/system/user']"));
        	Layout.reloadAjaxContent('/jump_success?url=/system/user');
        	return;
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}

// 保存并添加下一个用户
function add_next(callback){
	// Step1: 表单验证
	var valid = $("#user_add_form").valid();
	if(!valid){
		return;
	}
	var data = $('#user_add_form').serializeJson();
	var roleIds = data['roleIds'];
	if(!(roleIds instanceof Array)){
		var arr = [];
		arr.push(roleIds);
		data['roleIds'] = arr;
	}
	console.log(data);
	
	// Step2: 提交表单
    $.ajax({
    	url:"/system/user/add",
        type:"post",
        dataType:'json',
        contentType:"application/json;charset=utf-8",
        data:JSON.stringify(data),
        async:false,
        success:function(data){
        	if(data.code!='10000'){
        		parent.layer.alert(data.msg, {icon: 1});
        		return;
        	}
        	Layout.reloadAjaxContent('/system/user/add', $(".page-sidebar").find("a[href='/system/user']"));
        	return;
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}

function update_user(callback){
	// Step1: 表单验证
	// 校验基本表单
	var valid = $("#user_update_form").valid();
	// 校验省市区
	var district_valid = true;
	$("#user_update_province_id").parent().parent().find("label.error_tips").hide();
	var province = $("#user_update_province_id").find("option:checked").val();
	var city = $("#user_update_city_id").find("option:checked").val();
	var district = $("#user_update_district_id").find("option:checked").val();
	if(province==''|city==''|district==''){
		$("#user_update_province_id").parent().parent().find("label.error_tips").show();
		district_valid = false;
	}
	if(!valid | !district_valid){
		return;
	}
	var data = $('#user_update_form').serializeJson();
	var roleIds = data['roleIds'];
	if(!(roleIds instanceof Array)){
		var arr = [];
		arr.push(roleIds);
		data['roleIds'] = arr;
	}
	console.log(data);
	
	// Step2: 提交表单
    $.ajax({
    	url:"/system/user/update",
        type:"post",
        dataType:'json',
        contentType:"application/json;charset=utf-8",
        data:JSON.stringify(data),
        async:true,
        success:function(data){
        	if(data.code!='10000'){
        		parent.layer.alert(data.msg, {icon: 1});
        		return;
        	}
        	Layout.reloadAjaxContent('/jump_success?url=/system/user');
        	return;
        },
        error: function (response, ajaxOptions, thrownError) {
        	errorCallBack(response, ajaxOptions, thrownError);                
        }
    });
}