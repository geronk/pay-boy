!#/bin/bash

ps -ef |grep payboy |awk '{print $2}'|xargs kill -9

nohup java -Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=50005,suspend=n  -jar  -Djava.security.egd=file:/dev/./urandom  -Dspring.config.location=./application.yml,./application-prod.yml payboy.jar >> payboy.log  2>&1 &

tail -f payboy.log