/*
 * This file is generated by jOOQ.
*/
package com.ndood.merchant.jooq.tables.daos;


import com.ndood.merchant.jooq.tables.TDepartment;
import com.ndood.merchant.jooq.tables.records.TDepartmentRecord;

import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.10.8"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class TDepartmentDao extends DAOImpl<TDepartmentRecord, com.ndood.merchant.jooq.tables.pojos.TDepartment, Integer> {

    /**
     * Create a new TDepartmentDao without any configuration
     */
    public TDepartmentDao() {
        super(TDepartment.T_DEPARTMENT, com.ndood.merchant.jooq.tables.pojos.TDepartment.class);
    }

    /**
     * Create a new TDepartmentDao with an attached configuration
     */
    public TDepartmentDao(Configuration configuration) {
        super(TDepartment.T_DEPARTMENT, com.ndood.merchant.jooq.tables.pojos.TDepartment.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Integer getId(com.ndood.merchant.jooq.tables.pojos.TDepartment object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<com.ndood.merchant.jooq.tables.pojos.TDepartment> fetchById(Integer... values) {
        return fetch(TDepartment.T_DEPARTMENT.ID, values);
    }

    /**
     * Fetch a unique record that has <code>id = value</code>
     */
    public com.ndood.merchant.jooq.tables.pojos.TDepartment fetchOneById(Integer value) {
        return fetchOne(TDepartment.T_DEPARTMENT.ID, value);
    }

    /**
     * Fetch records that have <code>name IN (values)</code>
     */
    public List<com.ndood.merchant.jooq.tables.pojos.TDepartment> fetchByName(String... values) {
        return fetch(TDepartment.T_DEPARTMENT.NAME, values);
    }

    /**
     * Fetch a unique record that has <code>name = value</code>
     */
    public com.ndood.merchant.jooq.tables.pojos.TDepartment fetchOneByName(String value) {
        return fetchOne(TDepartment.T_DEPARTMENT.NAME, value);
    }

    /**
     * Fetch records that have <code>status IN (values)</code>
     */
    public List<com.ndood.merchant.jooq.tables.pojos.TDepartment> fetchByStatus(Integer... values) {
        return fetch(TDepartment.T_DEPARTMENT.STATUS, values);
    }

    /**
     * Fetch records that have <code>sort IN (values)</code>
     */
    public List<com.ndood.merchant.jooq.tables.pojos.TDepartment> fetchBySort(Integer... values) {
        return fetch(TDepartment.T_DEPARTMENT.SORT, values);
    }

    /**
     * Fetch records that have <code>parent_id IN (values)</code>
     */
    public List<com.ndood.merchant.jooq.tables.pojos.TDepartment> fetchByParentId(Integer... values) {
        return fetch(TDepartment.T_DEPARTMENT.PARENT_ID, values);
    }

    /**
     * Fetch records that have <code>create_time IN (values)</code>
     */
    public List<com.ndood.merchant.jooq.tables.pojos.TDepartment> fetchByCreateTime(LocalDateTime... values) {
        return fetch(TDepartment.T_DEPARTMENT.CREATE_TIME, values);
    }

    /**
     * Fetch records that have <code>update_time IN (values)</code>
     */
    public List<com.ndood.merchant.jooq.tables.pojos.TDepartment> fetchByUpdateTime(LocalDateTime... values) {
        return fetch(TDepartment.T_DEPARTMENT.UPDATE_TIME, values);
    }
}
