package com.ndood.merchant.core.exception;

import com.ndood.merchant.core.constaints.AdminErrCode;

public class MerchantException extends Exception {
	private static final long serialVersionUID = 13168926815908268L;

	private AdminErrCode code;

	public AdminErrCode getCode() {
		return code;
	}

	public void setCode(AdminErrCode code) {
		this.code = code;
	}

	public MerchantException(String message) {
		super(message);
	}

	public MerchantException(AdminErrCode code) {
        super(code.getValue());
        this.code = code;
    }
	
	public MerchantException(AdminErrCode code, String message) {
        super(message);
        this.code = code;
    }

}