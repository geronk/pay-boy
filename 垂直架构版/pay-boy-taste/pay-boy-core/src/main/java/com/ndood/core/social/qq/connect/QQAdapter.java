package com.ndood.core.social.qq.connect;
import org.springframework.social.connect.ApiAdapter;
import org.springframework.social.connect.ConnectionValues;
import org.springframework.social.connect.UserProfile;

import com.ndood.core.social.qq.api.QQ;
import com.ndood.core.social.qq.api.QQUserInfo;

/**
 * 创建QQ适配器类
 */
public class QQAdapter implements ApiAdapter<QQ>{

	/**
	 * 测试api
	 */
	@Override
	public boolean test(QQ api) {
		return true;
	}

	/**
	 * 设置ConnectionValues
	 */
	@Override
	public void setConnectionValues(QQ api, ConnectionValues values) {
		QQUserInfo userInfo = api.getUserInfo();
		values.setDisplayName(userInfo.getNickname()); // 用户名
		values.setImageUrl(userInfo.getFigureurl_qq_1()); // qq头像
		values.setProfileUrl(null); // 个人主页
		values.setProviderUserId(userInfo.getOpenId());
	}
	
	/**
	 * 设置UserProfile，QQ不需要这个(微博需要)
	 */
	@Override
	public UserProfile fetchUserProfile(QQ api) {
		return null;
	}

	/**
	 * 更新status, QQ不需要这个(微博需要)
	 */
	@Override
	public void updateStatus(QQ api, String message) {
		// do noting
	}
	
}