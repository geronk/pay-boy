package com.ndood.core.validate.code.image;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletWebRequest;

import com.ndood.core.validate.code.impl.AbstractValidateCodeProcessor;

/**
 * 图片验证码处理器
 */
@Component("imageValidateCodeProcessor")
public class ImageCodeProcessor extends AbstractValidateCodeProcessor<ImageCode> {

	/**
	 * 发送图形验证码，将其写到响应中
	 * @return 
	 * @return 
	 */
	@Override
	protected void send(ServletWebRequest request, ImageCode imageCode) throws Exception {
		// Step1: 设置请求头
		HttpServletResponse response = request.getResponse();
		response.setContentType("image/jpeg");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Cache-Control", "no-cache");
		response.setDateHeader("Expires", 0);
		
		// Step2：发送图片，并返回结果
		ImageIO.write(imageCode.getImage(), "JPEG", response.getOutputStream());
	}

}
