package com.ndood.payment.core.properties;

import lombok.Data;

@Data
public class SessionProperties {
	
	/**
	 * 是否允许所有的用户登录的键值
	 */
	private String contextKeyAllowGuest = "contextKeyAllowGuest";
	/**
	 * sessionInfo的key
	 */
	private String sessionUserInfoContextKey = "auth-session-userinfo";
	/**
	 * 存储客户端平台信息的 key, 存储的内容是 Platform
	 */
	private String clientPlatformContextKey = "auth-client-platform";
	/**
	 * 存储客户端版本信息的 key, 存储的内容是 *github.com/chanxuehong/util/version.Version
	 */
	private String clientVersionContextKey = "auth-client-version";
	
	/**
	 * 存储客户端设备ID信息的 key, 存储的内容是 string 类型的 DeviceID
	 */
	private String clientDeviceIDContextKey = "auth-client-device_id";
	
}
