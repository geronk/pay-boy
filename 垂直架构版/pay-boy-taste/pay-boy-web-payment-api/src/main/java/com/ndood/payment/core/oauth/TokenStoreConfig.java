package com.ndood.payment.core.oauth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.security.oauth2.provider.token.TokenStore;

/**
 * 将Inmemory方式改成token持久化方式
 */
@Configuration
public class TokenStoreConfig {
	
	/*@Autowired
	private DataSource dataSource;*/
	
	@Autowired
	private RedisConnectionFactory redisConnectionFactory;
	
	/**
	 * 产生token自动存到redis中
	 * 如果配置文件中的imooc.security.oauth2.storeType=redis时，这个配置生效
	 */
	@Bean
	public TokenStore redisTokenStore(){
		return new MyRedisTokenStore(redisConnectionFactory);
		//return new JdbcTokenStore(dataSource);
	}
	
}
