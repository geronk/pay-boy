package com.ndood.payment.core.properties;

import lombok.Data;

@Data
public class IdWorkerProperties {

	/**
	 * 分布式机器ID
	 */
	private Long workerId = 1L;
	/**
	 * 分布式数据中心ID
	 */
	private Long centerId = 1L;
	
}
