package com.ndood.payment.core.properties;

import lombok.Data;

@Data
public class AuthProperties {

	/**
	 * 小程序appid
	 */
	private String app_id;
	/**
	 * 小程序appsecret
	 */
	private String app_secret;
	/**
	 * 小程序providerid
	 */
	private String provider_id;
	/**
	 * 
	 */
	private String postProcessUrl = "/auth";
	/**
	 * clientId
	 */
	private String clientId = "starclient";
	/**
	 * clientSecret
	 */
	private String clientSecret = "starsecret";
	/**
	 * 超时时间（秒）
	 */
	private Integer expireIn = 24 * 60 * 60;
	
}
