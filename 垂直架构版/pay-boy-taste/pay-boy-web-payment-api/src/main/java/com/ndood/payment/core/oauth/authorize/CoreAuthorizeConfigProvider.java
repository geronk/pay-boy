package com.ndood.payment.core.oauth.authorize;

import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.stereotype.Component;

/**
 * 实现AuthorizeConfigProvider
 * 确保基本配置在最开始生效
 */
@Component
@Order(Integer.MIN_VALUE)
public class CoreAuthorizeConfigProvider implements AuthorizeConfigProvider{
	
	/**
	 * 抽取BrowserSecurityConfig ImoocResourceServerConfig的通用授权配置
	 */
	@Override
	public void config(ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry config) {
		config
			.antMatchers(
				"/img/**",
				"/js/**",
				"/css/**",
				"/favicon.ico",
				"/v3.0.0/star/peroid",
				"/v3.0.0/star/delete", // 删除测试用户数据
				"/v3.0.0/star/mock_share", // 模拟一个分享用户
				"/v3.0.0/star/share/picture**",
				"/v3.0.0/star/share/picture/**",
				"/v3.0.0/star/share/getwxacode**",
				"/v3.0.0/star/callback/wxpay",
				"/v3.0.0/star/project/popups",
				"/v3.0.0/star/banner/list",
				"/v3.0.0/star/config/defaultConfig",
				// 不拦截swagger的
				"/swagger-ui.html",
				"/webjars/**",
				"/swagger-resources/**",
				"/v2/api-docs",
				"/druid/**",
				"/demo/**")
			.permitAll();
	}
}
