package com.ndood.payment.core.properties;

/**
 * 自定义属性配置类SecurityProperties
 */
import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

/**
 * Star项目配置类
 */
@ConfigurationProperties(prefix = "com.ndood.payment")
@Data
public class PaymentProperties {

	/**
	 * session相关配置
	 */
	private SessionProperties sp = new SessionProperties();
	/**
	 * 支付相关配置
	 */
	private PayConfigProperties payment = new PayConfigProperties();
	/**
	 * 认证相关
	 */
	private AuthProperties auth = new AuthProperties();
	/**
	 * ID生成器相关
	 */
	private IdWorkerProperties id = new IdWorkerProperties();
	/**
	 * oss相关配置
	 */
	private AliyunOssProperties oss = new AliyunOssProperties();
}